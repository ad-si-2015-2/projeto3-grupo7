# Resumo
O projeto contém 3 jars sendo eles:
1. Servidor truco.jar: contém toda a lógica e as interfaces sockets e web servisse, 
2. cliente truco.jar: contém o cliente webservice com uma interface gráfica simples,
3. entidades truco.jar: contém as classes comuns ao servidor e cliente.

# Para Instalar e Executar Jogo
1. Acesse e baixe o .jar [JogodeTruco](https://gitlab.com/ad-si-2015-2/projeto3-grupo7/)
2. Sobre ordem de compilação
    2.1. Entidades jar : 
        2.1.1. Deve ser compilado primeiro por ser referenciado no cliente e servidor
    2.2. Referenciar aos outros jars : 
        2.1.1. Deve clicar com botão direito no projeto, depois em propriedades e na aba biblioteca deve adicionar a jar/pasta do entidades
    2.3 Compilar os outros jars
2. Sobre ordem de execução
    2.1. Servidor truco.jar : 
        2.1.1. Deve ser compilado primeiro e depois cliente truco.jar
