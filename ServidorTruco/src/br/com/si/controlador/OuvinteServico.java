package br.com.si.controlador;

/**
 * Classe observadora do controlador
 */
public interface OuvinteServico {

    /**
     * *
     * Notifica um ouvinte que um serviço foi executado
     *
     * @param ip requisitante
     * @param acao executada
     */
    public void servicoExecutado(String ip, String acao);
}
