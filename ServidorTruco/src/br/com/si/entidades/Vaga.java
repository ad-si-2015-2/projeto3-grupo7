package br.com.si.entidades;

import java.io.Serializable;
import java.util.Objects;

/**
 * Classe responsável por representar uma vaga no jogo de truco
 */
public class Vaga implements Serializable {

    private Jogador jogador;
    private boolean disponivel;
    private Dupla dupla;

    /**
     * Constroi uma vaga
     */
    public Vaga() {
        this.disponivel = true;
    }

    /*Setters*/
    /**
     * *
     *
     * @param jogador da vaga
     */
    public void setJogador(Jogador jogador) {
        this.jogador = jogador;
        this.disponivel = false;

    }

    /**
     *
     * @param disponivel seta a disponibilidade da vaga
     */
    public void setDisponivel(boolean disponivel) {
        this.disponivel = disponivel;
        if (disponivel) {
            this.jogador = null;
        }
    }

    /**
     *
     * @param dupla do jogo
     */
    public void setDupla(Dupla dupla) {
        this.dupla = dupla;
    }

    /*Getters*/
    /**
     *
     * @return jogador da vaga
     */
    public Jogador getJogador() {
        return jogador;
    }

    /**
     * *
     *
     * @return booleano indicando a disponibilidade da vaga
     */
    public boolean isDisponivel() {
        return disponivel;
    }

    /**
     *
     * @return Dupla1 ou Dupla2
     */
    public Dupla getDupla() {
        return dupla;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + Objects.hashCode(this.jogador);
        hash = 41 * hash + (this.disponivel ? 1 : 0);
        hash = 41 * hash + Objects.hashCode(this.dupla);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Vaga other = (Vaga) obj;
        if (this.disponivel != other.disponivel) {
            return false;
        }
        if (!Objects.equals(this.jogador, other.jogador)) {
            return false;
        }
        return this.dupla == other.dupla;
    }

    @Override
    /**
     * *
     * @return uma string representando a vaga.
     */
    public String toString() {
        return disponivel + " " + jogador + " " + dupla;
    }
}
