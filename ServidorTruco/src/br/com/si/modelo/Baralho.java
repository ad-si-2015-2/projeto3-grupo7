package br.com.si.modelo;

import br.com.si.entidades.Carta;
import br.com.si.entidades.Naipe;
import br.com.si.entidades.NumeroCarta;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe que representa um baralho de truco
 */
public class Baralho {

    private final List<Carta> cartas = new ArrayList<>();

    /**
     * Constroi o baralho de truco
     */
    public Baralho() {
        this.preencher();
    }

    /**
     * *
     * Preenche o baralho de truco
     */
    private void preencher() {

        /*Preenchendo cartas*/
        for (NumeroCarta n : NumeroCarta.values()) {
            if (n.ordinal() > 3) {

                for (Naipe na : Naipe.values()) {
                    cartas.add(new Carta(n, na, n.ordinal()));
                }
            }
        }
        /*Adicionando manilhas*/
        cartas.add(new Carta(NumeroCarta.SETE, Naipe.OURO, 11));//Sete ouro
        cartas.remove(new Carta(NumeroCarta.AS, Naipe.ESPADAS, 7));// Espadilha
        cartas.add(new Carta(NumeroCarta.AS, Naipe.ESPADAS, 12));// Espadilha
        cartas.add(new Carta(NumeroCarta.SETE, Naipe.COPAS, 13));// Sete copas
        cartas.add(new Carta(NumeroCarta.QUATRO, Naipe.PAUS, 14));// Zap  
    }

    /**
     * *
     *
     * @return cartas do baralho
     */
    public List<Carta> getCartas() {
        return this.cartas;
    }

    /**
     *
     * @param carta como string. Ex: 4P quatro de paus
     * @return o objeto carta
     */
    public Carta getCartaPorNome(String carta) {
        for (Carta c : new Baralho().getCartas()) {
            if (c.toString().equals(carta.toUpperCase())) {
                return c;
            }
        }
        return null;
    }
}
