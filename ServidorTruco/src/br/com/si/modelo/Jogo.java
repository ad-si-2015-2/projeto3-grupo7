package br.com.si.modelo;

import br.com.si.entidades.Dupla;
import br.com.si.entidades.Jogador;
import br.com.si.entidades.Vaga;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Classe que implementa as regras do jogo truco.
 *
 */
public class Jogo {

    /*Controle interno*/
    private final String mesa;
    private MaoTruco maoAtual;
    private final Map<Jogador, Dupla> jogadorDupla = new HashMap<>();
    private Dupla duplaVencedora;
    private final Jogador lider;
    private boolean FimDeJogo = false;
    private final List<MaoTruco> maos = new ArrayList<>();
    private final List<Vaga> vagas = new ArrayList<>(4);

    /**
     * Constroi o objeto jogo setando as 4 vagas disponíveis
     *
     * @param mesa Nome da mesa de truco
     * @param lider Jogador com previlégios de lider da partida
     * @param quantidadeVagas Quantidade de vagas do jogo
     */
    public Jogo(String mesa, Jogador lider, int quantidadeVagas) {

        if (quantidadeVagas != 2 && quantidadeVagas != 4) {
            throw new IllegalArgumentException("Quantidade de vagas invalida. Valores de 2 ou 4");
        }
        for (int i = 0; i < quantidadeVagas; i++) {
            vagas.add(new Vaga());
        }
        /*Definindo a vaga do lider*/
        Vaga vagaLider = vagas.get(0);
        vagaLider.setJogador(lider);
        vagaLider.setDisponivel(false);
        vagaLider.setDupla(Dupla.Dupla1);

        this.mesa = mesa;
        this.lider = lider;
    }

    /**
     * *
     * Inicia uma nova mão no jogo
     */
    public void novaMao() {
        maoAtual = new MaoTruco(this);
        maos.add(maoAtual);
    }

    /**
     *
     * @param dupla a qual a pontuação será retornada
     * @return o valor da pontuação de mão da dupla passada como parâmetro
     */
    public int getPontuacaoDupla(Dupla dupla) {
        int pontuacao = 0;
        for (MaoTruco m : maos) {
            if (dupla.equals(m.getDuplaVencedora())) {
                pontuacao += m.getValorAposta();
            }
        }
        return pontuacao;
    }

    /**
     * *
     *
     * @return retorna o jogador lider da rodada
     */
    public Jogador getLider() {
        return lider;
    }

    /*Regras do jogo funcionamento*/
    /**
     * *
     * Inicia o jogo se todas as vagas estão preenchidas e duplas montadas.
     *
     * @param lider o jogador que iniciará a partida
     * @throws IllegalArgumentException quando o jogador tentar inciar o jogo e
     * não for o lider
     * @throws IllegalStateException quando o jogo não possuir vagas.
     * @throws IllegalStateException quando as duplas não forem formadas
     * corregamente.
     */
    public void iniciar(Jogador lider) {
        if (!lider.equals(this.lider)) {
            throw new ModeloException("Usuário nao autorizado a iniciar o jogo. Somente o lider pode faze-lo.");
        }
        int i = 0;
        //Vagas não preenchidas
        for (Vaga v : this.vagas) {
            if (v.isDisponivel()) {
                throw new ModeloException("Jogo não iniciado pois há vagas disponíveis");
            }
            if (v.getDupla().equals(Dupla.Dupla1)) {
                i++;
            } else {
                i--;
            }
        }
        //Duplas não formadas
        if (i != 0) {
            throw new ModeloException("As duplas ainda não foram formadas");
        }
        /*Associando os jogadores a suas duplas*/
        this.vagas.stream().forEach((v) -> {
            jogadorDupla.put(v.getJogador(), v.getDupla());
        });
        novaMao();
    }

    /**
     * *
     * Responsável por desconectar um jogador do jogo
     *
     * @param jogador que irá deixar o jogo
     */
    public void sairDoJogo(Jogador jogador) {
        Dupla duplaJogador = jogadorDupla.get(jogador);
        /*Removendo o jogador da associação jogador/Dupla*/
        this.jogadorDupla.remove(jogador);
        /*Removendo a vaga do jogador*/
        vagas.stream().filter((v) -> (v.getJogador().equals(jogador))).forEach((v) -> {
            v.setDisponivel(true);
        });
        /*O jogo acaba*/
        FimDeJogo = true;
        /*A dupla vencedora é a que o jogador não saiu*/
        duplaVencedora = duplaJogador.equals(Dupla.Dupla1) ? Dupla.Dupla2 : Dupla.Dupla1;
    }

    /**
     * *
     * Conecta um jogador no jogo de truco.
     *
     * @param jogador a ser conectado ao jogo
     */
    public void entrarNoJogo(Jogador jogador) {

        for (int i = 0; i < vagas.size(); i++) {
            if (vagas.get(i).isDisponivel()) {
                vagas.get(i).setJogador(jogador);
                vagas.get(i).setDisponivel(false);
                vagas.get(i).setDupla(vagas.get(i - 1).getDupla().equals(Dupla.Dupla1) ? Dupla.Dupla2 : Dupla.Dupla1);
                return;
            }
        }
        throw new ModeloException("Não há vagas nesse jogo");
    }

    /**
     *
     * @param dupla da pontuação
     * @return pontuação da dupla passada como parâmetro
     */
    public int getPontuacaoMaoDupla(Dupla dupla) {
        int pontuacao = 0;
        pontuacao = maos.stream().filter((m) -> (m.getDuplaVencedora() != null && dupla.equals(m.getDuplaVencedora()))).map((m) -> m.getValorAposta()).reduce(pontuacao, Integer::sum);
        return pontuacao;
    }

    /**
     * seta a variável fim de jogo com base na pontuação das duplas
     */
    public void setFimDeJogo() {
        FimDeJogo = getPontuacaoDupla(Dupla.Dupla1) == 12 || getPontuacaoDupla(Dupla.Dupla2) == 12;
        if (FimDeJogo) {
            if (getPontuacaoDupla(Dupla.Dupla1) == 12) {
                duplaVencedora = Dupla.Dupla1;
            } else {
                duplaVencedora = Dupla.Dupla2;
            }
        }
    }

    /*Getters do jogo*/
    /**
     * @return Nome da mesa
     */
    public String getMesa() {
        return mesa;
    }

    /**
     *
     * @return Número da mão atual
     */
    public int getNumeroMao() {
        return maos.size();
    }

    /**
     *
     * @return Mapa de jogador-dupla. Quatro jogadores para duas duplas.
     */
    public Map<Jogador, Dupla> getJogadorDupla() {
        return jogadorDupla;
    }

    /**
     *
     * @return Dupla vencedora da mão. Se não terminou a rodada o valor será
     * nulo.
     */
    public Dupla getDuplaVencedora() {
        return duplaVencedora;
    }

    /**
     *
     * @return True se o jogo acabou.
     */
    public boolean isFimDeJogo() {
        return FimDeJogo;
    }

    /**
     *
     * @return Lista de todas as vagas do jogo.
     */
    public List<Vaga> getVagas() {
        return vagas;
    }

    /**
     *
     * @return mão atual do jogo
     */
    public MaoTruco getMaoAtual() {
        return maoAtual;
    }

    /**
     *
     * @return todos os jogadores do jogo
     */
    public List<Jogador> getJogadores() {
        List<Jogador> jogadores = new ArrayList<>();
        vagas.stream().forEach((v) -> {
            if (v.getJogador() != null) {
                jogadores.add(v.getJogador());
            }
        });
        return jogadores;
    }

    /**
     *
     * @param ip do jogador
     * @return jogador com o referido ip
     */
    public Jogador getJogadorPorIp(String ip) {
        for (Vaga v : vagas) {
            if (v.getJogador().getIp().equals(ip)) {
                return v.getJogador();
            }
        }
        return null;
    }

    /**
     *
     * @param jogador jogador para obter o parceiro
     * @return jogador parceiro
     */
    public Jogador getJogadorParceiro(Jogador jogador) {
        for (Jogador j : getJogadores()) {
            /* Se o jogador da coleção tem a mesma dupla do jogador parâmetro e eles não são o mesmo, trata-se do parceiro*/
            if (jogadorDupla.get(jogador).equals(jogadorDupla.get(j)) && !j.equals(jogador)) {
                return j;
            }
        }
        return null;
    }

    /**
     *
     * @param dupla para obtenção dos jogadores
     * @return jogadores da dupla
     */
    public List<Jogador> getJogadoresPorDupla(Dupla dupla) {
        List<Jogador> jogadores = new ArrayList<>();
        getJogadores().stream().filter((j) -> (jogadorDupla.get(j).equals(dupla))).forEach((j) -> {
            jogadores.add(j);
        });
        /* Se o jogador da coleção tem a mesma dupla do jogador parâmetro e eles não são o mesmo, trata-se do parceiro*/
        return jogadores;
    }
}
