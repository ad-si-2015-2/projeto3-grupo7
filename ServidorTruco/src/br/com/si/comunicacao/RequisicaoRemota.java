package br.com.si.comunicacao;

import br.com.si.controlador.ControladorTruco;
import br.com.si.entidades.ServicoTruco;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import javax.jws.WebService;

/**
 * Classe que representa um cliente remoto do servidor. Com o objetivo de obter
 * os dados da solicitação, passá-los ao controlador principal e enviar a
 * resposta ao cliente
 *
 */
@WebService(endpointInterface = "br.com.si.entidades.ServicoTruco")
public class RequisicaoRemota implements Runnable, ServicoTruco {

    private Servidor servidor;
    private String ipCliente;

    public RequisicaoRemota() {
    }

    /**
     * *
     * Contrutor do cliente remoto
     *
     * @param servidor que contém os canais de comunicação com o cliente
     * @param ipCliente Ip da máquina solicitante.
     * @throws java.io.IOException
     */
    public RequisicaoRemota(Servidor servidor, String ipCliente) throws IOException {
        this.servidor = servidor;
        this.ipCliente = ipCliente;
        PrintStream p = new PrintStream(servidor.getCanalDeSaida(ipCliente));
        p.println("Digite .ajuda para informacoes sobre o jogo.");
    }

    /**
     * O cliente informa ao sokete um fluxo de caracteres que pode conter o
     * caracter backSpace Dessa formar torna-se necessário retirá-lo
     *
     * @param string a qual se quer remover os caracteres especiais
     * @return string sem os caracteres especiais
     */
    private String removeBackSpace(String string) {

        ArrayList<Character> caracteresValidos = new ArrayList<>();
        Character[] caracteresOriginais = new Character[string.length()];

        int i = 0;
        for (char c : string.toCharArray()) {
            caracteresOriginais[i++] = c;
        }
        caracteresValidos.addAll(Arrays.asList(caracteresOriginais));

        for (int k = 0, j = 0; k < string.length(); k++) {
            if (caracteresOriginais[k] == '\b') {
                caracteresValidos.remove(k - j++);
                if (k - j + 1 > 0) {
                    caracteresValidos.remove(k - j++);
                }
            }
        }
        char[] chars = new char[caracteresValidos.size()];
        for (int z = 0; z < chars.length; z++) {
            chars[z] = caracteresValidos.get(z);
        }

        return String.valueOf(chars);
    }

    /**
     * *
     * Executa o fluxo a cada solicitação do cliente
     */
    @Override
    public void run() {
        try {
            Scanner s = new Scanner(servidor.getCanalDeEntrada(ipCliente));
            while (s.hasNextLine()) {
                String comando = s.nextLine();
                comando = removeBackSpace(comando);
                ControladorTruco.obterInstancia().servico(ipCliente, comando);
            }
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }

    @Override
    public void executarAcao(Integer identificador, String comando) {
        ControladorTruco.obterInstanciaWebService().servico(identificador.toString(), comando);
    }

    @Override
    public String getMensagens(Integer identificador) {
        String mensagem = "";
        for (String s : ControladorTruco.obterInstanciaWebService().getMensagens().get(identificador.toString())) {
            mensagem = mensagem + s + "\n";
        }
        ControladorTruco.obterInstanciaWebService().getMensagens().get(identificador.toString()).clear();
        return mensagem;
    }

    @Override
    public Integer gerarIdentificador(String nomeJogador) {
        Integer identificador = ControladorTruco.obterInstanciaWebService().getIdentificador(nomeJogador);
        return identificador;
    }
}
